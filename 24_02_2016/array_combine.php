<?php

/*
  $a = array('Name', 'Address', 'Email');
  $b = array('shahinur alam', 'Mohakhali,Dhaka', 'shahintc2013@gmail.com');
  $c = array_combine($a,$b);
  print_r($c);

  echo '</br>';

  //or you can wright this mathod bellow

  $x = array('Name','Address','Email');
  $y = array('shahin khan','Mohakahli, Dhaka','shahintc2013@gmail.com');
  print_r(array_combine($x, $y));

 */



$a = array('name', 'address', 'phone');
$b = array('shahin', 'mohakhali', 01620929252);
echo "<pre>";
print_r(array_combine($a, $b));
echo "</pre>";

/*OUTPUT:
Array
(
    [name] => shahin
    [address] => mohakhali
    [phone] => 912
)
*/


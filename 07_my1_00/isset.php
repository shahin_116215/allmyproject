<?php
//isset - determine if a variable is set and is not null
$var = '';
//this will evaluate to true so the text will be printed .
if (isset($var)){
	
	echo "this var is set so i wull print";
	
}
//in the next example we'll use var_damp to output

$a="test";
$b= "anothertest";

var_dump(isset($a));//true
var_dump(isset($a,$b));//true

unset ($a);

var_dump(isset($a));//false
var_dump(isset($a,$b));//false

$foo =NULL;
var_dump(isset($foo));//false

?>
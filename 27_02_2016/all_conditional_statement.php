<?php

//if-if_else:


$a = 10;
if ($a == 10) {

    echo "\$a is small than 10";
} else {


    echo '$a is not small than 15';
}

//OUTPUT:$a is small than 15
echo "</br></br></br></br>";

//if_elseif_else:

$a = 10;
$b = 15;
$c = 20;
$d = 25;
if ($a == $b) {
    echo "$a and $b equal";
} elseif ($a == $b) {
    echo "$a and $b are equal";
} elseif ($a == $c) {
    echo "$a and $c are equal";
} elseif ($a == $d) {
    echo "$a and $d are equal";
} else {
    echo "no value aren't equal";
}
//OUTPUT:no value aren't equal

echo "</br></br></br></br>";
//switch:


$shahin = 10;
switch ($shahin) {
    case 1:
        echo "my name is shahin khan case-1";
        break;
    case 3:
        echo "my name is shahin khan case-3";
        break;
    case 6:
        echo "my name is shahin khan case-6";
        break;
    case 8:
        echo "my name is shahin khan case-8";
        break;
    case 10:
        echo "my name is shahin khan case-10";
        break;
}
//OUTPUT:my name is shahin khan case-10
    
